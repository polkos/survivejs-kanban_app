import React from 'react';

export default class Note extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      editing: false
    };
  }

  render() {
    if (this.state.editing) {
      return this.renderEdit();
    }

    return this.renderNote();
  }

  renderEdit = () =>
    <input type="text"
           ref={(e) => e ? e.selectionStart = this.props.task.length : null}
           autoFocus={true}
           defaultValue={this.props.task}
           onBlur={this.finishEdit}
           onKeyPress={this.checkEnter} />;

  renderNote = () =>
    <div onClick={this.edit}>
      <span>{this.props.task}</span>
      {this.props.onDelete ? this.renderDelete() : null }
    </div>;

  renderDelete = () =>
    <button onClick={this.props.onDelete}>x</button>;

  edit = () => {
    this.setState({ editing: true });
  };

  checkEnter = (e) => {
    if(e.key === 'Enter') {
      this.finishEdit(e);
    }
  };

  finishEdit = (e) => {
    // A smarter way to deal with the default value would be to set
    // it through `defaultProps`.
    if(this.props.onEdit) {
      this.props.onEdit(e.target.value);

      this.setState({
        editing: false
      });
    }
  };
}